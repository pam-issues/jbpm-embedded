package com.example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.jbpm.kie.services.impl.KModuleDeploymentService;
import org.jbpm.kie.services.impl.KModuleDeploymentUnit;
import org.jbpm.kie.services.impl.ProcessServiceImpl;
import org.jbpm.kie.services.impl.RuntimeDataServiceImpl;
import org.jbpm.kie.services.impl.bpmn2.BPMN2DataServiceImpl;
import org.jbpm.kie.services.impl.query.QueryServiceImpl;
import org.jbpm.runtime.manager.impl.RuntimeManagerFactoryImpl;
import org.jbpm.runtime.manager.impl.jpa.EntityManagerFactoryManager;
import org.jbpm.services.api.DefinitionService;
import org.jbpm.services.api.ProcessService;
import org.jbpm.services.api.model.DeployedUnit;
import org.jbpm.services.api.model.DeploymentUnit;
import org.jbpm.services.task.HumanTaskServiceFactory;
import org.jbpm.services.task.audit.TaskAuditServiceFactory;
import org.jbpm.shared.services.impl.TransactionalCommandService;
import org.jbpm.test.services.TestIdentityProvider;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.task.TaskService;
import org.kie.internal.runtime.manager.context.EmptyContext;
import org.kie.test.util.db.PersistenceUtil;

/**
 * Hello world!
 *
 */
public class App {
	private DefinitionService bpmn2Service;
	private KModuleDeploymentService deploymentService;
	private ProcessService processService;
	private DeploymentUnit deploymentUnit;

	public static void main(String[] args) {
		try {
			System.out.println("Embedded jBPM");

			App app = new App();
			app.init();
			app.deploy();
			app.startProcess();

			//Thread.currentThread().join();
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	void init() {
		// emf
		EntityManagerFactory emf = getEmf("org.jbpm.persistence.jpa");
		EntityManagerFactoryManager.get()
		                           .addEntityManagerFactory("org.jbpm.domain", emf);

		// build definition service
		bpmn2Service = new BPMN2DataServiceImpl();

		TestIdentityProvider identityProvider = new TestIdentityProvider();

		// Deployment Service
		deploymentService = new KModuleDeploymentService();
		deploymentService.setBpmn2Service(bpmn2Service);
		deploymentService.setManagerFactory(new RuntimeManagerFactoryImpl());
		deploymentService.setEmf(emf);
		deploymentService.setIdentityProvider(identityProvider);

		TaskService taskService = HumanTaskServiceFactory.newTaskServiceConfigurator()
		                                                 .entityManagerFactory(emf)
		                                                 .getTaskService();

		// build runtime data service
		RuntimeDataServiceImpl runtimeDataService = new RuntimeDataServiceImpl();

		runtimeDataService.setCommandService(new TransactionalCommandService(emf));
		runtimeDataService.setIdentityProvider(identityProvider);
		runtimeDataService.setTaskService(taskService);
		// runtimeDataService.setDeploymentRolesManager(deploymentRolesManager);
		runtimeDataService.setTaskAuditService(TaskAuditServiceFactory.newTaskAuditServiceConfigurator()
		                                                              .setTaskService(taskService)
		                                                              .getTaskAuditService());

		// set runtime
		deploymentService.setRuntimeDataService(runtimeDataService);

		// set runtime data service as listener on deployment service
		((KModuleDeploymentService) deploymentService).addListener(((RuntimeDataServiceImpl) runtimeDataService));
		((KModuleDeploymentService) deploymentService).addListener(((BPMN2DataServiceImpl) bpmn2Service));

		// process service
		processService = new ProcessServiceImpl();
		((ProcessServiceImpl) processService).setDataService(runtimeDataService);
		((ProcessServiceImpl) processService).setDeploymentService(deploymentService);
	}

	EntityManagerFactory getEmf(String name) {
		Properties properties = new Properties();
		properties.put("driverClassName", "org.h2.Driver");
		properties.put("className", "org.h2.jdbcx.JdbcDataSource");
		properties.put("user", "sa");
		properties.put("password", "sa");
		properties.put("url", "jdbc:h2:file:~/pam-h2");
		properties.put("datasourceName", "jdbc/jbpm-ds");
		PersistenceUtil.setupPoolingDataSource(properties);
		Map<String, String> map = new HashMap<String, String>();
		map.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
		return Persistence.createEntityManagerFactory(name, map);
	}

	void deploy() {
		deploymentUnit = new KModuleDeploymentUnit("com.example", "simple", "1.0.0-SNAPSHOT");

		// deploy the unit
		deploymentService.deploy(deploymentUnit);
	}

	void startProcess() {
		// get the deploymendId for the deployed unit
		String deploymentId = deploymentUnit.getIdentifier();
		// retrieve the deployed unit
		DeployedUnit deployed = deploymentService.getDeployedUnit(deploymentId);
		// get the runtime manager
		RuntimeManager manager = deployed.getRuntimeManager();
		RuntimeEngine runtimeEngine = manager.getRuntimeEngine(EmptyContext.get());

		ProcessInstance piid = runtimeEngine.getKieSession()
		                                    .startProcess("simpleHT");

		List<Long> tasks = runtimeEngine.getTaskService()
										.getTasksByProcessInstanceId(piid.getId());
										
		System.out.println("tasks: "+tasks);
		// long processInstanceId =
		// processService.startProcess(deploymentUnit.getIdentifier(), "Simple.simple");

		// ProcessInstance pi = processService.getProcessInstance(processInstanceId);
		manager.disposeRuntimeEngine(runtimeEngine);
	}
}
